package services

import (
	"errors"

	"github.com/getsentry/sentry-go"
	"github.com/go-playground/validator"
	"github.com/google/uuid"
	"go.uber.org/zap"
	"opencitylabs.it/fakeregistryproxy/appcontext"
	"opencitylabs.it/fakeregistryproxy/internal/core/domain"
	"opencitylabs.it/fakeregistryproxy/internal/core/ports"
	"opencitylabs.it/fakeregistryproxy/logger"
	"opencitylabs.it/fakeregistryproxy/sentryutils"
	"opencitylabs.it/fakeregistryproxy/utils"
)

type protocollerService struct {
	log                *zap.Logger
	sentryHub          *sentry.Hub
	validator          *validator.Validate
	producerRepo       ports.DocumentProducerRepository
	protocolSystemRepo ports.ProtocolSystem
	storage            ports.StorageRepository
	appContext         appcontext.AppContext
}

func NewProtocollerService(ac appcontext.AppContext, producerRepo ports.DocumentProducerRepository, protocolSystemRepo ports.ProtocolSystem, storageRepo ports.StorageRepository) ports.ProtocollerServiceService {
	return &protocollerService{
		log:                logger.GetLogger(),
		sentryHub:          sentryutils.InitLocalSentryhub(),
		validator:          validator.New(),
		producerRepo:       producerRepo,
		protocolSystemRepo: protocolSystemRepo,
		storage:            storageRepo,
		appContext:         ac,
	}
}

func (r *protocollerService) ProcessMessage(document domain.Document) error {
	databaseSync := r.appContext.GetDatabaseSync()
	databaseSync.RLock()
	defer databaseSync.RUnlock()

	if !r.shouldProcessThisDocument(&document) {
		return errors.New("config not found")
	}

	err := r.validateDocumentMessage(&document)
	if err != nil {
		_ = r.producerRepo.ProduceOnRetryQueue(document)
		return err
	}

	if r.isDocumentProtocolled(&document) {
		return errors.New("already protocolled")
	}
	if r.isDocInCreatedStatus(&document) {
		r.setEventField(&document)
		document.Status = "REGISTRATION_PENDING"
		err = r.producerRepo.ProduceMessage(document)
		if err != nil {
			return err
		}
		return nil
	}

	protocolNumber, err := r.protocolSystemRepo.GenerateProtocolDocument(document)
	if err != nil {
		_ = r.producerRepo.ProduceOnRetryQueue(document)
		return err
	}

	r.setRegistrationData(&document, protocolNumber)
	r.setEventField(&document)
	document.Status = "REGISTRATION_COMPLETE"
	err = r.producerRepo.ProduceMessage(document)
	if err != nil {
		return err
	}

	return nil
}

func (r *protocollerService) validateDocumentMessage(document *domain.Document) error {

	err := r.validator.Struct(document)
	if err != nil {
		return err
	}
	return nil
}

func (r *protocollerService) setRegistrationData(document *domain.Document, protocolNumber string) {
	document.FolderInfo.ID = "n/a"
	registrationdata := domain.RegistrationInfo{}
	registrationdata.DocumentNumber = protocolNumber
	document.RegistrationData = &registrationdata
	registrationDate := utils.GetCurrentDateTime()
	document.RegistrationData.Date = &registrationDate
	document.RegistrationData.TransmissionType = r.setRegistrationType(document)

}
func (r *protocollerService) setRegistrationType(document *domain.Document) string {
	if document.SourceType == "tenant" && document.RecipientType == "internal" {
		return "Internal"
	} else if document.SourceType == "tenant" && document.RecipientType == "user" {
		return "Outbound"
	} else if document.SourceType == "user" && document.RecipientType == "tenant" {
		return "Inbound"
	}
	return ""
}
func (r *protocollerService) setEventField(doc *domain.Document) {
	//version
	doc.Event_Version = 1
	//app id
	doc.AppId = "fake-registry-proxy:" + r.appContext.GetConfig().App.AppVersion
	//event_created_at and UpdatedAt
	doc.EventCreatedAt = utils.GetCurrentDateTime()
	doc.UpdatedAt = doc.EventCreatedAt
	//event_id
	newUuidEventId := uuid.NewString()
	doc.EventId = newUuidEventId
}

func (r *protocollerService) isDocumentProtocolled(document *domain.Document) bool {
	if document.RegistrationData != nil {
		return document.RegistrationData.DocumentNumber != ""
	}
	return false

}

func (r *protocollerService) isDocInCreatedStatus(document *domain.Document) bool {
	if document.Status == "DOCUMENT_CREATED" {
		return true
	}
	return false

}

func (r *protocollerService) shouldProcessThisDocument(document *domain.Document) bool {
	return true && r.shouldProcessThisService(document.RemoteCollection.ID) && r.shouldProcessThisTenant(document.TenantID)
}

func (r *protocollerService) shouldProcessThisTenant(tenantid string) bool {
	tenantConfig, err := r.storage.GetTenantById(tenantid)
	if err != nil {
		r.log.Error("error retrieving tenantConfig with id: "+tenantid+" ", zap.Error(err))
		return false
	}
	if tenantConfig.ID == "" {
		return false
	}
	return true
}

func (r *protocollerService) shouldProcessThisService(serviceId string) bool {
	serviceConfig, err := r.storage.GetServiceById(serviceId)
	if err != nil {
		r.log.Error("error retrieving serviceConfig with id: "+serviceId+" ", zap.Error(err))
		return false
	}
	if serviceConfig.ID == "" || !serviceConfig.IsActive {
		return false
	}
	return true
}
