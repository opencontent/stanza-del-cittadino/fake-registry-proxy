package services

import (
	"testing"

	"github.com/go-playground/validator"
	"opencitylabs.it/fakeregistryproxy/internal/core/domain"
	"opencitylabs.it/fakeregistryproxy/logger"
	"opencitylabs.it/fakeregistryproxy/sentryutils"
)

func TestIsDocumentProtocolled(t *testing.T) {

	logger := logger.GetLogger()
	defer logger.Sync()

	registryProxy := protocollerService{
		log:       logger,
		sentryHub: sentryutils.InitLocalSentryhub(),
		validator: validator.New(),
	}
	fakeNum := "fakeNum"
	documentMock := domain.Document{
		RegistrationData: &domain.RegistrationInfo{
			DocumentNumber: fakeNum,
		},
	}

	result := registryProxy.isDocumentProtocolled(&documentMock)

	if result != true {
		t.Fatal("Expected true got false")
	}

	emptyDocumentMock := domain.Document{}
	result = registryProxy.isDocumentProtocolled(&emptyDocumentMock)

	if result != false {
		t.Fatal("Expected false got true")
	}

}
