package services

import (
	"encoding/json"
	"time"

	"github.com/getsentry/sentry-go"
	"go.uber.org/zap"
	"opencitylabs.it/fakeregistryproxy/appcontext"
	"opencitylabs.it/fakeregistryproxy/internal/core/domain"
	"opencitylabs.it/fakeregistryproxy/internal/core/ports"
	"opencitylabs.it/fakeregistryproxy/logger"
	"opencitylabs.it/fakeregistryproxy/sentryutils"
)

type configManagerService struct {
	log        *zap.Logger
	sentryHub  *sentry.Hub
	storage    ports.StorageRepository
	appContext appcontext.AppContext
}

func NewConfigManagerService(ac appcontext.AppContext, storageRepo ports.StorageRepository) ports.ConfigManagerService {
	return &configManagerService{
		log:        logger.GetLogger(),
		sentryHub:  sentryutils.InitLocalSentryhub(),
		storage:    storageRepo,
		appContext: ac,
	}
}

// tenants
func (r *configManagerService) GetTenantConfigList(offset, limit int) (domain.TenantConfigList, error) {
	storageSync := r.appContext.GetCloudStorageSync()
	storageSync.RLock()
	defer storageSync.RUnlock()

	tenantConfigList := domain.TenantConfigList{}
	var tenantArray []domain.TenantConfig

	tenantArray, err := r.storage.GetTenantConfigList(offset, limit)
	if err != nil {
		r.log.Debug("failed retrieving tenant list from storage" + err.Error())
		return tenantConfigList, err
	}
	if len(tenantArray) == 0 {
		tenantConfigList.Results = []domain.TenantConfig{}
	} else {

		tenantConfigList.Results = append(tenantConfigList.Results, tenantArray...)
	}
	tenantConfigList.Count = len(tenantArray)
	return tenantConfigList, nil
}

func (r *configManagerService) StoreTenantConfig(tenant domain.TenantConfig) error {
	storageSync := r.appContext.GetCloudStorageSync()
	storageSync.Lock()
	defer storageSync.Unlock()

	err := r.storage.StoreTenantConfig(tenant)
	if err != nil {
		r.log.Debug("failed storing tenant to storage" + err.Error())
		return err
	}
	return nil
}

func (r *configManagerService) RemoveTenantConfig(tenantId string) error {
	storageSync := r.appContext.GetCloudStorageSync()
	storageSync.Lock()
	defer storageSync.Unlock()

	err := r.storage.RemoveTenantConfig(tenantId)
	if err != nil {
		r.log.Debug("failed removing tenant from storage" + err.Error())
		return err
	}
	return nil
}

func (r *configManagerService) GetTenantById(tenantId string) (domain.TenantConfig, error) {
	storageSync := r.appContext.GetCloudStorageSync()
	storageSync.RLock()
	defer storageSync.RUnlock()
	tenantConfig, err := r.storage.GetTenantById(tenantId)
	if err != nil {
		r.log.Error("error retrieving tenantConfig with id: "+tenantId+" ", zap.Error(err))
		return tenantConfig, err
	}
	return tenantConfig, nil
}

func (r *configManagerService) UpdateTenantConfig(tenant domain.TenantConfig) (domain.TenantConfig, error) {
	storageSync := r.appContext.GetCloudStorageSync()
	storageSync.Lock()
	defer storageSync.Unlock()

	currentTime := time.Now()
	tenant.ModifiedAt = currentTime.Format("2006-01-02T15:04:05-07:00")

	tenantconfig, err := r.storage.UpdateTenantConfig(tenant)
	if err != nil {
		return tenantconfig, err
	}
	return tenantconfig, nil
}

// services
func (r *configManagerService) GetServiceConfigList() (domain.ServiceConfigList, error) {
	storageSync := r.appContext.GetCloudStorageSync()
	storageSync.RLock()
	defer storageSync.RUnlock()

	var serviceConfigList domain.ServiceConfigList

	serviceArray, err := r.storage.GetServiceConfigList()
	if err != nil {
		r.log.Debug("failed retrieving service list from storage" + err.Error())
		return serviceConfigList, err
	}
	serviceConfigList.Count = len(serviceArray)
	serviceConfigList.Results = serviceArray

	return serviceConfigList, nil
}
func (r *configManagerService) StoreServiceConfig(service domain.ServiceConfig) error {
	r.log.Debug("StoreServiceConfig 1")
	storageSync := r.appContext.GetCloudStorageSync()
	storageSync.Lock()
	defer storageSync.Unlock()
	err := r.storage.StoreServiceConfig(service)
	if err != nil {
		r.log.Debug("StoreServiceConfig 2")
		r.log.Debug("failed storing service to storage" + err.Error())
		return err
	}
	r.log.Debug("StoreServiceConfig 3")
	return nil
}
func (r *configManagerService) RemoveServiceConfig(serviceId string) error {
	err := r.storage.RemoveServiceConfig(serviceId)
	if err != nil {
		r.log.Debug("failed removing service from storage" + err.Error())
		return err
	}
	return nil
}

func (r *configManagerService) GetServiceById(serviceId string) (domain.ServiceConfig, error) {
	storageSync := r.appContext.GetCloudStorageSync()
	storageSync.RLock()
	defer storageSync.RUnlock()

	serviceConfig, err := r.storage.GetServiceById(serviceId)
	if err != nil {
		r.log.Error("error retrieving serviceConfig with id: "+serviceId+" ", zap.Error(err))
		return serviceConfig, err
	}
	return serviceConfig, nil
}

func (r *configManagerService) UpdateServiceConfig(service domain.ServiceConfig) (domain.ServiceConfig, error) {
	storageSync := r.appContext.GetCloudStorageSync()
	storageSync.Lock()
	defer storageSync.Unlock()

	modifiedService, err := r.storage.UpdateServiceConfig(service)
	if err != nil {
		return modifiedService, err
	}
	return modifiedService, nil
}

// schema
func (r *configManagerService) GetSchemaConfig() (domain.SchemaConfig, error) {
	storageSync := r.appContext.GetCloudStorageSync()
	storageSync.RLock()
	defer storageSync.RUnlock()
	schemaConfigStr := `{
		"display": "form",
		"settings": null,
		"components": [
		  {
			"label": "UUID dell'ente",
			"placeholder": "XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX",
			"applyMaskOn": "change",
			"hidden": true,
			"tableView": true,
			"validate": {
			  "required": true
			},
			"key": "tenant_id",
			"type": "textfield",
			"input": true
		  },
		  {
			"label": "UUID del Servizio",
			"placeholder": "XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX",
			"applyMaskOn": "change",
			"hidden": true,
			"tableView": true,
			"validate": {
			  "required": true
			},
			"key": "id",
			"type": "textfield",
			"input": true
		  },
		  {
			"label": "Descrizione del servizio",
			"placeholder": "Comune di Bugliano",
			"applyMaskOn": "change",
			"hidden": true,
			"tableView": true,
			"validate": {
			  "required": true
			},
			"key": "description",
			"type": "textfield",
			"input": true,
			"defaultValue": "test"
		  },
		  {
			"label": "Impostazioni specifiche del protocollo",
			"tableView": false,
			"validate": {
			  "required": false
			},
			"key": "registry",
			"type": "container",
			"input": true,
			"components": [
			  {
				"label": "URL Protocollazione",
				"placeholder": "http://www.example.com/",
				"tableView": true,
				"validate": {
				  "required": true
				},
				"key": "url",
				"type": "textfield",
				"input": true
			  },
			  {
				"label": "Gerarchia di Classificazione",
				"placeholder": "1.2.3",
				"tableView": true,
				"validate": {
				  "required": false
				},
				"key": "classification_hierarchy",
				"type": "textfield",
				"input": true
			  },
			  {
				"label": "Numero Fascicolo",
				"placeholder": "1",
				"tableView": true,
				"validate": {
				  "required": false
				},
				"key": "folder_number",
				"type": "textfield",
				"input": true
			  },
			  {
				"label": "Anno Fascicolo",
				"placeholder": "2022",
				"tableView": true,
				"validate": {
				  "required": false
				},
				"key": "folder_year",
				"type": "textfield",
				"input": true
			  }
			]
		  },
		  {
			"label": "Attivo",
			"tableView": false,
			"validate": {
			  "required": false
			},
			"key": "is_active",
			"type": "checkbox",
			"input": true,
			"defaultValue": false
		  },
		  {
			"label": "Salva",
			"tableView": false,
			"validate": {
			  "required": false
			},
			"key": "submit",
			"type": "button",
			"input": true,
			"disableOnInvalid": true
		  }
		]
	  }`

	var schemaConfig domain.SchemaConfig
	err := json.Unmarshal([]byte(schemaConfigStr), &schemaConfig)
	return schemaConfig, err
}
