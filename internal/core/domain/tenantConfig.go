package domain

type TenantConfig struct {
	ID              string `json:"id" example:"12"`
	Description     string `json:"description" example:"Random Tenant Description 111"`
	Slug            string `json:"slug" example:"random-tenant-slug"`
	BaseURL         string `json:"base_url" example:"https://sdc.example.com"`
	InstitutionCode string `json:"institution_code" example:"random-institution-code"`
	AooCode         string `json:"aoo_code" example:"random-aoo-code"`
	CreatedAt       string `json:"created_at" example:"2023-06-05"`
	ModifiedAt      string `json:"modified_at" example:"2023-08-07"`
}

type TenantConfigList struct {
	Count    int            `json:"count" example:"1"`
	Self     *string        `json:"self" example:"fake self"`
	Next     *string        `json:"next" example:"fake next"`
	Previous *string        `json:"previous" example:"fake previous"`
	Results  []TenantConfig `json:"results"`
}
