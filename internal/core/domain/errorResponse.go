package domain

type Error struct {
	Type     string `json:"type"`
	Title    string `json:"title"`
	Status   int    `json:"status,omitempty"`
	Detail   string `json:"detail"`
	Instance string `json:"instance,omitempty"`
}

type ErrorResponse struct {
	Errors []Error `json:"errors"`
}
