package domain

type ServiceConfig struct {
	Registry    Registry `json:"registry"`
	IsActive    bool     `json:"is_active"`
	Submit      bool     `json:"submit"`
	TenantID    string   `json:"tenant_id"`
	ID          string   `json:"id"`
	Description string   `json:"description"`
}

type Registry struct {
	URL                     string `json:"url" example:"http://example.com"`
	ClassificationHierarchy string `json:"classification_hierarchy" example:"example_hierarchy"`
	FolderNumber            string `json:"folder_number" example:"example_folder_number"`
	FolderYear              string `json:"folder_year" example:"2023"`
}

type ServiceConfigList struct {
	Count   int             `json:"count" example:"5"`
	Results []ServiceConfig `json:"results"`
}

type Data struct {
	TenantID    string   `json:"tenant_id"`
	ID          string   `json:"id"`
	Description string   `json:"description"`
	Registry    Registry `json:"registry"`
	IsActive    bool     `json:"is_active"`
	Submit      bool     `json:"submit"`
}
