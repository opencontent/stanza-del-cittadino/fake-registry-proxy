package domain

type SchemaConfig struct {
	Display    string `json:"display" example:"Example Display"`
	Settings   any    `json:"settings" example:"{}"`
	Components []struct {
		Label       string `json:"label" example:"Example Label"`
		Placeholder string `json:"placeholder,omitempty" example:"Example Placeholder"`
		ApplyMaskOn string `json:"applyMaskOn,omitempty" example:"Example Mask"`
		Hidden      bool   `json:"hidden,omitempty" example:"false"`
		TableView   bool   `json:"tableView" example:"true"`
		ClearOnHide bool   `json:"clearOnHide,omitempty" example:"false"`
		Validate    struct {
			Required bool `json:"required" example:"true"`
		} `json:"validate,omitempty"`
		Key          string      `json:"key" example:"example_key"`
		Type         string      `json:"type" example:"text"`
		Input        bool        `json:"input" example:"true"`
		DefaultValue interface{} `json:"defaultValue,omitempty" example:"null"`
		Components   []struct {
			Label       string `json:"label" example:"Nested Label"`
			Placeholder string `json:"placeholder" example:"Nested Placeholder"`
			TableView   bool   `json:"tableView" example:"false"`
			Validate    struct {
				Required bool `json:"required" example:"false"`
			} `json:"validate,omitempty"`
			Key         string `json:"key" example:"nested_key"`
			Type        string `json:"type" example:"nested_text"`
			Input       bool   `json:"input" example:"true"`
			ClearOnHide bool   `json:"clearOnHide,omitempty" example:"false"`
		} `json:"components,omitempty"`
		ShowValidations  bool `json:"showValidations,omitempty" example:"true"`
		DisableOnInvalid bool `json:"disableOnInvalid,omitempty" example:"true"`
		SaveOnEnter      bool `json:"saveOnEnter,omitempty" example:"true"`
	} `json:"components"`
}

// Settings fields of SchemaConfig struct is of type "any". Swaggo doesn't support that type to
// generate swagger doc example. So for now, we can use the SchemaConfigSwaggerDoc struct with "setting" field
// of type "*string" in order to genate the documentation.
// todo: valuate if we can modify the   SchemaConfig struct in the same way and avoid to duplicate the code
// using SchemaConfigSwaggerDoc struct just for documentation
type SchemaConfigSwaggerDoc struct {
	Display    string  `json:"display" example:"Example Display"`
	Settings   *string `json:"settings" example:"{}"`
	Components []struct {
		Label       string `json:"label" example:"Example Label"`
		Placeholder string `json:"placeholder,omitempty" example:"Example Placeholder"`
		ApplyMaskOn string `json:"applyMaskOn,omitempty" example:"Example Mask"`
		Hidden      bool   `json:"hidden,omitempty" example:"false"`
		TableView   bool   `json:"tableView" example:"true"`
		ClearOnHide bool   `json:"clearOnHide,omitempty" example:"false"`
		Validate    struct {
			Required bool `json:"required" example:"true"`
		} `json:"validate,omitempty"`
		Key          string `json:"key" example:"example_key"`
		Type         string `json:"type" example:"text"`
		Input        bool   `json:"input" example:"true"`
		DefaultValue string `json:"defaultValue,omitempty" example:"null"`
		Components   []struct {
			Label       string `json:"label" example:"Nested Label"`
			Placeholder string `json:"placeholder" example:"Nested Placeholder"`
			TableView   bool   `json:"tableView" example:"false"`
			Validate    struct {
				Required bool `json:"required" example:"false"`
			} `json:"validate,omitempty"`
			Key         string `json:"key" example:"nested_key"`
			Type        string `json:"type" example:"nested_text"`
			Input       bool   `json:"input" example:"true"`
			ClearOnHide bool   `json:"clearOnHide,omitempty" example:"false"`
		} `json:"components,omitempty"`
		ShowValidations  bool `json:"showValidations,omitempty" example:"true"`
		DisableOnInvalid bool `json:"disableOnInvalid,omitempty" example:"true"`
		SaveOnEnter      bool `json:"saveOnEnter,omitempty" example:"true"`
	} `json:"components"`
}
