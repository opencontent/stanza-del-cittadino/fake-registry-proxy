package ports

import "opencitylabs.it/fakeregistryproxy/internal/core/domain"

// driver ports
type ProtocollerServiceService interface {
	ProcessMessage(document domain.Document) error
}
type ConfigManagerService interface {
	//tenants
	GetTenantConfigList(offset, limit int) (domain.TenantConfigList, error)
	StoreTenantConfig(tenant domain.TenantConfig) error
	RemoveTenantConfig(tenantId string) error
	GetTenantById(tenantId string) (domain.TenantConfig, error)
	UpdateTenantConfig(tenant domain.TenantConfig) (domain.TenantConfig, error)
	//services
	GetServiceConfigList() (domain.ServiceConfigList, error)
	StoreServiceConfig(service domain.ServiceConfig) error
	RemoveServiceConfig(serviceId string) error
	GetServiceById(serviceId string) (domain.ServiceConfig, error)
	UpdateServiceConfig(service domain.ServiceConfig) (domain.ServiceConfig, error)
	//schema
	GetSchemaConfig() (domain.SchemaConfig, error)
}

// driven ports
type DocumentProducerRepository interface {
	ProduceMessage(document domain.Document) error
	ProduceOnRetryQueue(document domain.Document) error
	CloseKafkaWriter()
}

type ProtocolSystem interface {
	GenerateProtocolDocument(document domain.Document) (string, error)
}

type StorageRepository interface {
	//tenants
	GetTenantConfigList(offset, limit int) ([]domain.TenantConfig, error)
	StoreTenantConfig(tenant domain.TenantConfig) error
	RemoveTenantConfig(tenantId string) error
	GetTenantById(tenantId string) (domain.TenantConfig, error)
	UpdateTenantConfig(tenant domain.TenantConfig) (domain.TenantConfig, error)
	//services
	GetServiceConfigList() ([]domain.ServiceConfig, error)
	StoreServiceConfig(service domain.ServiceConfig) error
	RemoveServiceConfig(tserviceId string) error
	GetServiceById(serviceId string) (domain.ServiceConfig, error)
	UpdateServiceConfig(service domain.ServiceConfig) (domain.ServiceConfig, error)
}
