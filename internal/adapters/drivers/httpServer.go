package drivers

import (
	"context"
	"fmt"
	"io"
	"net/http"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	swaggerfiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
	"go.uber.org/zap"
	"opencitylabs.it/fakeregistryproxy/config"
	docs "opencitylabs.it/fakeregistryproxy/docs"
	"opencitylabs.it/fakeregistryproxy/internal/core/domain"
	"opencitylabs.it/fakeregistryproxy/internal/core/ports"
	"opencitylabs.it/fakeregistryproxy/logger"
	"opencitylabs.it/fakeregistryproxy/utils"
)

type registryProxyServer struct {
	log           *zap.Logger
	conf          *config.Config
	srv           *gin.Engine
	configManager ports.ConfigManagerService
	ctx           context.Context
}

func NewRegistryProxyServer(configManager ports.ConfigManagerService, ctx context.Context) *registryProxyServer {
	s := &registryProxyServer{
		log:           logger.GetLogger(),
		conf:          config.GetConfig(),
		configManager: configManager,
		ctx:           ctx,
	}
	if !s.conf.Server.DebugMode {
		gin.SetMode(gin.ReleaseMode)
		gin.DefaultWriter = io.Discard
	} else {
		gin.SetMode(gin.DebugMode)
	}
	s.srv = gin.Default()
	s.srv.ForwardedByClientIP = true
	s.setSwaggerDoc()
	s.initRoutes()

	return s
}

func (r *registryProxyServer) initRoutes() {
	r.srv.GET("/status", r.healthCheckHandler)
	r.srv.GET("/metrics", r.getPrometheusMetrics)

	r.srv.GET("/v1/tenants/", r.getConfigTenantList)
	r.srv.POST("/v1/tenants/", r.tenantsCreate)
	r.srv.GET("/v1/tenants/:id", r.tenantsRetrieveById)
	r.srv.PUT("/v1/tenants/:id", r.tenantsUpdate)
	r.srv.PATCH("/v1/tenants/:id", r.tenantsUpdate)
	r.srv.DELETE("/v1/tenants/:id", r.tenantsDestroy)

	r.srv.GET("/v1/schema", r.getConfigSchema)

	r.srv.GET("/v1/services/:id", r.servicesRetrieveById)
	r.srv.OPTIONS("/v1/services/:id", r.servicesRetrieveById)
	r.srv.POST("/v1/services/", r.servicesCreate)
	r.srv.PUT("/v1/services/:id", r.servicesUpdate)
	r.srv.PATCH("/v1/services/:id", r.servicesUpdate)
	r.srv.DELETE("/v1/services/:id", r.servicesDestroy)

	r.srv.GET("/docs/*any", ginSwagger.WrapHandler(swaggerfiles.Handler))
}

func (r *registryProxyServer) Run() {
	r.srv.Run(r.conf.Server.AddressPort)
}

// healthCheckHandler godoc
// @Summary Check the health of the Kafka broker.
// @Description This endpoint checks the health of at least one Kafka broker.
// @Tags health-check
// @Accept json
// @Produce json
// @Success 200 {string} string "OK"
// @Router /status [get]
func (r *registryProxyServer) healthCheckHandler(c *gin.Context) {

	c.Header("Content-Type", "application/json")
	if IsAtleastOneKafkaBrokerHealth(r.ctx, r.conf.Kafka.KafkaServer) {
		c.String(http.StatusOK, "OK")
	} else {
		c.JSON(http.StatusInternalServerError, gin.H{
			"internal error ": "failed to connect to Kafka",
		})
		return
	}

}

// getPrometheusMetrics retrieves Prometheus metrics.
// @Summary Get Prometheus Metrics
// @Description Retrieve Prometheus metrics data.
// @Produce text/plain
// @Success 200 {string} string "Prometheus Metrics"
// @Router /metrics [get]
func (r *registryProxyServer) getPrometheusMetrics(c *gin.Context) {
	promHandler := gin.WrapH(promhttp.Handler())
	promHandler(c)
}

// getConfigTenantList godoc
// @Summary Get the list of tenant configurations.
// @Description This endpoint retrieves the list of tenant configurations. Please note that this API uses pagination, indicating the URLs for the previous and next pages.
// @Tags Tenants
// @Accept json
// @Produce json
// @Success 200 {object} domain.TenantConfigList "tenant list config info"
// @Failure 400 {object} string  "Bad Request"
// @Failure 404 {object} string  "Bad Request"
// @Failure 500 {object} string  "Internal Server Error"
// @Router /tenants [get]
func (r *registryProxyServer) getConfigTenantList(c *gin.Context) {
	offset, _ := strconv.Atoi(c.Query("offset"))
	limit, _ := strconv.Atoi(c.Query("limit"))
	sort := c.Query("sort")

	if limit == 0 {
		limit = 10
	}

	tenantConfingList, err := r.configManager.GetTenantConfigList(offset, limit)
	tenatList := tenantConfingList.Results
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"internal error": err.Error(),
		})
		return
	}

	if sort == "" {
		sort = "createdAt"
	}
	var selfPage, nextPage, prevPage string
	selfPage = fmt.Sprintf("/tenants/?offset=%d&limit=%d&sort=%s", offset, len(tenatList), sort)

	if len(tenatList) >= limit {
		nextPage = fmt.Sprintf("/tenants/?offset=%d&limit=%d&sort=%s", offset+limit, limit, sort)
	}
	if offset > 0 {
		prevPage = fmt.Sprintf("/tenants/?offset=%d&limit=%d&sort=%s", offset-limit, limit, sort)
	}

	var nextURL *string
	var prevURL *string
	var selfURL *string

	endpoint := r.conf.Server.AddressPort
	if nextPage != "" {
		tmp := "http://" + endpoint + nextPage
		nextURL = &tmp
	}
	if prevPage != "" {
		tmp := "http://" + endpoint + prevPage
		prevURL = &tmp
	}
	if selfPage != "" {
		tmp := "http://" + endpoint + selfPage
		selfURL = &tmp
	}

	tenantConfingList.Self = selfURL
	tenantConfingList.Next = nextURL
	tenantConfingList.Previous = prevURL

	c.JSON(http.StatusOK, tenantConfingList)
}

// tenantsCreate godoc
// @Summary Create a new tenant configuration.
// @Description This endpoint creates a new tenant configuration.
// @Tags Tenants
// @Accept json
// @Produce json
// @Param request body domain.TenantConfig true "New tenant configuration"
// @Success 201 {object} domain.TenantConfig "Created tenant configuration"
// @Failure 400 {object} string "Bad Request"
// @Failure 500 {object} string "Internal Server Error"
// @Router /tenants [post]
func (r *registryProxyServer) tenantsCreate(c *gin.Context) {
	var newTenant domain.TenantConfig
	c.Header("Content-Type", "application/json")
	if err := c.ShouldBindJSON(&newTenant); err != nil {
		errorResponse := domain.ErrorResponse{}
		errorData := domain.Error{
			Type:   "Bad Request",
			Title:  "Request malformed",
			Status: http.StatusBadRequest,
			Detail: err.Error(),
		}
		errorResponse.Errors = append(errorResponse.Errors, errorData)
		c.JSON(http.StatusBadRequest, errorResponse)
		return
	}
	currentTime := time.Now()
	newTenant.CreatedAt = currentTime.Format("2006-01-02T15:04:05-07:00")
	newTenant.ModifiedAt = currentTime.Format("2006-01-02T15:04:05-07:00")
	err := r.configManager.StoreTenantConfig(newTenant)
	if err != nil && err.Error() == "object already exists in backing store (use store.Get)" {
		errorResponse := domain.ErrorResponse{}
		errorData := domain.Error{
			Type:   "object conflict",
			Title:  "object already exists in backing store",
			Status: http.StatusConflict,
			Detail: err.Error(),
		}
		errorResponse.Errors = append(errorResponse.Errors, errorData)
		c.JSON(http.StatusConflict, errorResponse)
		return
	}
	if err != nil {
		errorResponse := domain.ErrorResponse{}
		errorData := domain.Error{
			Type:   "Internal error",
			Title:  "Internal error",
			Status: http.StatusInternalServerError,
			Detail: err.Error(),
		}
		errorResponse.Errors = append(errorResponse.Errors, errorData)
		c.JSON(http.StatusInternalServerError, errorResponse)
		return
	}
	c.JSON(http.StatusCreated, newTenant)
}

// tenantsRetrieveById godoc
// @Summary Retrieve a specific tenant configuration by ID.
// @Description This endpoint retrieves a specific tenant configuration by its ID.
// @Tags Tenants
// @Accept json
// @Produce json
// @Param id path string true "Tenant ID"
// @Success 200 {object} domain.TenantConfig "tenant config info"
// @Failure 400 {object} string "Bad Request"
// @Failure 404 {object} string "Not Found"
// @Failure 500 {object} string "Internal Server Error"
// @Router /tenants/{id} [get]
func (r *registryProxyServer) tenantsRetrieveById(c *gin.Context) {
	tenantConfig, err := r.configManager.GetTenantById(c.Param("id"))
	c.Header("Content-Type", "application/json")
	if err != nil {
		errorResponse := domain.ErrorResponse{}
		errorData := domain.Error{
			Type:   "Internal error",
			Title:  "Internal error",
			Status: http.StatusInternalServerError,
			Detail: err.Error(),
		}
		errorResponse.Errors = append(errorResponse.Errors, errorData)
		// Return the error response as JSON
		c.JSON(http.StatusInternalServerError, errorResponse)
		return
	}

	tenantConfigStr, err := utils.GetJSONTenantConfigFromStruct(tenantConfig)
	if err != nil {

		errorResponse := domain.ErrorResponse{}
		errorData := domain.Error{
			Type:   "Internal error",
			Title:  "Internal error",
			Status: http.StatusInternalServerError,
			Detail: err.Error(),
		}
		errorResponse.Errors = append(errorResponse.Errors, errorData)
		// Return the error response as JSON
		c.JSON(http.StatusInternalServerError, errorResponse)
		return
	}
	if tenantConfig.ID == "" {
		errorResponse := domain.ErrorResponse{}
		errorData := domain.Error{
			Type:   "object not found",
			Title:  "object not found in store",
			Status: http.StatusNotFound,
			Detail: "object not found in store",
		}
		errorResponse.Errors = append(errorResponse.Errors, errorData)
		c.JSON(http.StatusNotFound, errorResponse)
		return
	}
	c.String(http.StatusOK, tenantConfigStr)
}

// tenantsUpdate godoc
// @Summary Update a tenant configuration by ID.
// @Description This endpoint updates a specific tenant configuration by its ID.
// @Tags Tenants
// @Accept json
// @Produce json
// @Param id path string true "Tenant ID"
// @Param request body domain.TenantConfig true "New tenant configuration"
// @Success 201 {object} domain.TenantConfig "Updated tenant configuration"
// @Failure 400 {object} string "Bad Request"
// @Failure 500 {object} string "Internal Server Error"
// @Router /tenants/{id} [put]
func (r *registryProxyServer) tenantsUpdate(c *gin.Context) {
	var newTenantConf domain.TenantConfig

	c.Header("Content-Type", "application/json")
	if err := c.ShouldBindJSON(&newTenantConf); err != nil {
		errorResponse := domain.ErrorResponse{}
		errorData := domain.Error{
			Type:   "Bad Request",
			Title:  "Request malformed",
			Status: http.StatusBadRequest,
			Detail: err.Error(),
		}
		errorResponse.Errors = append(errorResponse.Errors, errorData)
		c.JSON(http.StatusBadRequest, errorResponse)
		return
	}
	newTenantConf.ID = c.Param("id")
	modifiedTenant, err := r.configManager.UpdateTenantConfig(newTenantConf)
	if err != nil {
		errorResponse := domain.ErrorResponse{}
		errorData := domain.Error{
			Type:   "object not found",
			Title:  "object not found in store",
			Status: http.StatusNotFound,
			Detail: "object not found in store",
		}
		errorResponse.Errors = append(errorResponse.Errors, errorData)
		c.JSON(http.StatusNotFound, errorResponse)
		return
	}
	c.JSON(http.StatusCreated, modifiedTenant)
}

// tenantsDestroy godoc
// @Summary Delete a tenant configuration by ID.
// @Description This endpoint deletes a specific tenant configuration by its ID.
// @Tags Tenants
// @Param id path string true "Tenant ID"
// @Success 204 "Tenant configuration deleted"
// @Failure 400 {object} string "Bad Request"
// @Failure 500 {object} string "Internal Server Error"
// @Router /tenants/{id} [delete]
func (r *registryProxyServer) tenantsDestroy(c *gin.Context) {
	c.Header("Content-Type", "application/json")

	err := r.configManager.RemoveTenantConfig(c.Param("id"))
	if err != nil && err.Error() == "object not found" {
		r.log.Debug("error value: " + err.Error())
		errorResponse := domain.ErrorResponse{}
		errorData := domain.Error{
			Type:   "object not found",
			Title:  "object not found in store",
			Status: http.StatusNotFound,
			Detail: "object not found in store",
		}
		errorResponse.Errors = append(errorResponse.Errors, errorData)
		c.JSON(http.StatusNotFound, errorResponse)
		return
	}

	c.Status(http.StatusNoContent)
}

// servicesCreate godoc
// @Summary Create a new service configuration.
// @Description This endpoint creates a new service configuration.
// @Tags Services
// @Accept json
// @Produce json
// @Param request body domain.ServiceConfig true "New service configuration"
// @Success 201 {object} domain.ServiceConfig "Created service configuration"
// @Failure 400 {object} string "Bad Request"
// @Failure 500 {object} string "Internal Server Error"
// @Router /services [post]
func (r *registryProxyServer) servicesCreate(c *gin.Context) {
	var newService domain.ServiceConfig

	c.Header("Content-Type", "application/json")
	if err := c.ShouldBindJSON(&newService); err != nil {
		errorResponse := domain.ErrorResponse{}
		errorData := domain.Error{
			Type:   "Bad Request",
			Title:  "Request malformed",
			Status: http.StatusBadRequest,
			Detail: err.Error(),
		}
		errorResponse.Errors = append(errorResponse.Errors, errorData)
		c.JSON(http.StatusBadRequest, errorResponse)
		return
	}
	r.log.Debug("input payload service: " + newService.ID)
	r.log.Debug("input payload service: " + newService.TenantID)
	err := r.configManager.StoreServiceConfig(newService)
	if err != nil && err.Error() == "object already exists in backing store (use store.Get)" {
		errorResponse := domain.ErrorResponse{}
		errorData := domain.Error{
			Type:   "object conflict",
			Title:  "object already exists in backing store",
			Status: http.StatusConflict,
			Detail: err.Error(),
		}
		errorResponse.Errors = append(errorResponse.Errors, errorData)
		c.JSON(http.StatusConflict, errorResponse)
		return
	}
	if err != nil {
		errorResponse := domain.ErrorResponse{}
		errorData := domain.Error{
			Type:   "Internal error",
			Title:  "Internal error",
			Status: http.StatusInternalServerError,
			Detail: err.Error(),
		}
		errorResponse.Errors = append(errorResponse.Errors, errorData)
		c.JSON(http.StatusInternalServerError, errorResponse)
		return
	}
	c.JSON(http.StatusCreated, newService)
}

// servicesRetrieveById godoc
// @Summary Retrieve a specific service configuration by ID.
// @Description This endpoint retrieves a specific service configuration by its ID.
// @Tags Services
// @Accept json
// @Produce json
// @Param id path string true "Service ID"
// @Success 200 {object} domain.ServiceConfig "Service configuration"
// @Failure 400 {object} string "Bad Request"
// @Failure 500 {object} string "Internal Server Error"
// @Router /services/{id} [get]
func (r *registryProxyServer) servicesRetrieveById(c *gin.Context) {

	c.Header("Content-Type", "application/json")
	serviceConfig, err := r.configManager.GetServiceById(c.Param("id"))
	if err != nil {
		errorResponse := domain.ErrorResponse{}
		errorData := domain.Error{
			Type:   "Internal error",
			Title:  "Internal error",
			Status: http.StatusInternalServerError,
			Detail: err.Error(),
		}
		errorResponse.Errors = append(errorResponse.Errors, errorData)
		// Return the error response as JSON
		c.JSON(http.StatusInternalServerError, errorResponse)
		return
	}
	serviceConfigStr, err := utils.GetJSONServiceConfigFromStruct(serviceConfig)
	if err != nil {

		errorResponse := domain.ErrorResponse{}
		errorData := domain.Error{
			Type:   "Internal error",
			Title:  "Internal error",
			Status: http.StatusInternalServerError,
			Detail: err.Error(),
		}
		errorResponse.Errors = append(errorResponse.Errors, errorData)
		// Return the error response as JSON
		c.JSON(http.StatusInternalServerError, errorResponse)
		return
	}
	if serviceConfig.ID == "" {
		errorResponse := domain.ErrorResponse{}
		errorData := domain.Error{
			Type:   "object not found",
			Title:  "object not found in store",
			Status: http.StatusNotFound,
			Detail: "object not found in store",
		}
		errorResponse.Errors = append(errorResponse.Errors, errorData)
		c.JSON(http.StatusNotFound, errorResponse)
		return
	}
	c.String(http.StatusOK, serviceConfigStr)
}

// servicesUpdate godoc
// @Summary Update a service configuration by ID.
// @Description This endpoint updates a specific service configuration by its ID.
// @Tags Services
// @Accept json
// @Produce json
// @Param id path string true "Service ID"
// @Param request body domain.ServiceConfig true "New service configuration"
// @Success 201 {object} domain.ServiceConfig "Updated service configuration"
// @Failure 400 {object} string "Bad Request"
// @Failure 500 {object} string "Internal Server Error"
// @Router /services/{id} [put]
func (r *registryProxyServer) servicesUpdate(c *gin.Context) {
	var newServiceConf domain.ServiceConfig

	c.Header("Content-Type", "application/json")
	if err := c.ShouldBindJSON(&newServiceConf); err != nil {
		errorResponse := domain.ErrorResponse{}
		errorData := domain.Error{
			Type:   "Bad Request",
			Title:  "Request malformed",
			Status: http.StatusBadRequest,
			Detail: err.Error(),
		}
		errorResponse.Errors = append(errorResponse.Errors, errorData)
		c.JSON(http.StatusBadRequest, errorResponse)
		return
	}
	newServiceConf.ID = c.Param("id")
	modifiedService, err := r.configManager.UpdateServiceConfig(newServiceConf)
	if err != nil {
		errorResponse := domain.ErrorResponse{}
		errorData := domain.Error{
			Type:   "object not found",
			Title:  "object not found in store",
			Status: http.StatusNotFound,
			Detail: "object not found in store",
		}
		errorResponse.Errors = append(errorResponse.Errors, errorData)
		c.JSON(http.StatusNotFound, errorResponse)
		return
	}
	c.JSON(http.StatusCreated, modifiedService)
}

// servicesDestroy godoc
// @Summary Delete a service configuration by ID.
// @Description This endpoint deletes a specific service configuration by its ID.
// @Tags Services
// @Accept json
// @Produce json
// @Param id path string true "Service ID"
// @Success 204 "Service configuration deleted"
// @Failure 400 {object} string "Bad Request"
// @Failure 500 {object} string "Internal Server Error"
// @Router /services/{id} [delete]
func (r *registryProxyServer) servicesDestroy(c *gin.Context) {

	c.Header("Content-Type", "application/json")
	err := r.configManager.RemoveServiceConfig(c.Param("id"))
	if err != nil && err.Error() == "object not found" {
		r.log.Debug("error value: " + err.Error())
		errorResponse := domain.ErrorResponse{}
		errorData := domain.Error{
			Type:   "object not found",
			Title:  "object not found in store",
			Status: http.StatusNotFound,
			Detail: "object not found in store",
		}
		errorResponse.Errors = append(errorResponse.Errors, errorData)
		c.JSON(http.StatusNotFound, errorResponse)
		return
	}

	c.Status(http.StatusNoContent)
}

// getConfigSchema godoc
// @Summary Get the schema configuration.
// @Description This endpoint retrieves the schema configuration.
// @Tags Schema
// @Accept json
// @Produce json
// @Success 200 {object} domain.SchemaConfigSwaggerDoc "JSON representation of schema configuration"
// @Failure 400 {object} string "Bad Request"
// @Failure 500 {object} string "Internal Server Error"
// @Router /schema [get]
func (r *registryProxyServer) getConfigSchema(c *gin.Context) {

	c.Header("Content-Type", "application/json")
	schemaConfig, err := r.configManager.GetSchemaConfig()
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"internal error ": err.Error(),
		})
		return
	}

	schemaConfigStr, err := utils.GetJSONSchemaConfigFromStruct(schemaConfig)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"internal error ": err.Error(),
		})
		return
	}
	c.String(http.StatusOK, schemaConfigStr)
}

func (r *registryProxyServer) setSwaggerDoc() {

	docs.SwaggerInfo.BasePath = r.conf.Server.AddressPort
	v1 := r.srv.Group("Health-Check")
	{
		eg := v1.Group("/Health-Check")
		{
			eg.GET("/status", r.healthCheckHandler)
		}
	}
	v2 := r.srv.Group("tenants")
	{
		eg := v2.Group("/tenants config")
		{
			eg.GET("/tenants/", r.getConfigTenantList)
			eg.POST("/tenants/", r.tenantsCreate)
			eg.GET("/tenants/:id", r.tenantsRetrieveById)
			eg.PUT("/tenants/:id", r.tenantsUpdate)
			eg.PATCH("/tenants/:id", r.tenantsUpdate)
			eg.DELETE("/tenants/:id", r.tenantsDestroy)
		}
	}
	v3 := r.srv.Group("services")
	{
		eg := v3.Group("/services config")
		{
			eg.GET("/services/:id", r.servicesRetrieveById)
			eg.OPTIONS("/services/:id", r.servicesRetrieveById)
			eg.POST("/services/", r.servicesCreate)
			eg.PUT("/servapiices/:id", r.servicesUpdate)
			eg.PATCH("/services/:id", r.servicesUpdate)
			eg.DELETE("/services/:id", r.servicesDestroy)
		}
	}
	v4 := r.srv.Group("schema")
	{
		eg := v4.Group("/schema config")
		{
			eg.GET("/schema", r.getConfigSchema)
		}
	}
	v5 := r.srv.Group("metrics")
	{
		eg := v5.Group("/metrics")
		{
			eg.GET("/metrics", r.getPrometheusMetrics)
		}
	}

}
