package drivers

import (
	"context"
	"encoding/json"
	"errors"
	"strconv"
	"time"

	"github.com/getsentry/sentry-go"
	"github.com/segmentio/kafka-go"
	"go.uber.org/zap"
	"opencitylabs.it/fakeregistryproxy/config"
	"opencitylabs.it/fakeregistryproxy/internal/core/ports"
	"opencitylabs.it/fakeregistryproxy/logger"
	"opencitylabs.it/fakeregistryproxy/metrics"
	"opencitylabs.it/fakeregistryproxy/sentryutils"
	"opencitylabs.it/fakeregistryproxy/utils"
)

type KafkaConsumer struct {
	KafkaReader *kafka.Reader
	log         *zap.Logger
	config      *config.Config
	service     ports.ProtocollerServiceService
	sentryHub   *sentry.Hub
}

func NewKafkaConsumer(kafkaServer []string, kafkaTopic, consumerGroup string, service ports.ProtocollerServiceService) *KafkaConsumer {
	r := &KafkaConsumer{
		log:       logger.GetLogger(),
		config:    config.GetConfig(),
		service:   service,
		sentryHub: sentryutils.InitLocalSentryhub(),
	}
	r.KafkaReader = r.getKafkaReader(kafkaServer, kafkaTopic, consumerGroup)
	return r
}

func (r *KafkaConsumer) getKafkaReader(kafkaServer []string, kafkaTopic, consumerGroup string) *kafka.Reader {
	r.log.Sugar().Debug("Connecting to topic: ", kafkaTopic, " from kafka server ", kafkaServer)
	reader := kafka.NewReader(kafka.ReaderConfig{
		Brokers: kafkaServer,
		Topic:   kafkaTopic,
		GroupID: consumerGroup,
		MaxWait: 3 * time.Second,
	})
	return reader
}

func (r *KafkaConsumer) CloseKafkaReader() {
	err := r.KafkaReader.Close()
	if err != nil {
		r.log.Error("Error closing consumer: ", zap.Error(err))
		sentry.CaptureException(err)
		return
	}
	r.log.Debug("Consumer closed")
}

func (r *KafkaConsumer) ProcessMessage(ctx context.Context) {
	for {
		m, err := r.KafkaReader.FetchMessage(ctx) // read without committing
		if err != nil {
			r.log.Info("error reading message from kafka: ", zap.Error(err))
			r.log.Error("error reading message from kafka: ", zap.Error(err))
			metrics.OpsFailedProcessed.WithLabelValues(r.config.App.Environment, "empty").Inc()
			commitMessage(r.log, ctx, r.KafkaReader, m, "", "", r.config.App.Environment)
			continue
		}
		document, err := utils.GetDocumentStruct(m.Value)
		if err != nil {
			r.log.Sugar().Info("error building document struct: ", zap.Error(err))
			r.log.Sugar().Error("error building document struct: ", zap.Error(err))
			sentry.CaptureException(errors.New("error building document struct"))
			metrics.OpsFailedProcessed.WithLabelValues(r.config.App.Environment, "empty").Inc()
			commitMessage(r.log, ctx, r.KafkaReader, m, "", "", r.config.App.Environment)
			continue

		}

		if document.Event_Version != config.SUPPORTED_EVENT_VERSION {
			metrics.OpsSuccessProcessed.WithLabelValues(r.config.App.Environment, document.TenantID).Inc()
			r.log.Sugar().Info("ENV:" + r.config.App.Environment + " Document : " + document.ID + ", discarded: event version not equal to " + strconv.Itoa(config.SUPPORTED_EVENT_VERSION))
			commitMessage(r.log, ctx, r.KafkaReader, m, document.ID, document.TenantID, r.config.App.Environment)
			continue
		}
		err = r.service.ProcessMessage(document)
		if err != nil && (err.Error() == "config not found" || err.Error() == "already protocolled") {
			metrics.OpsSuccessProcessed.WithLabelValues(r.config.App.Environment, document.TenantID).Inc()
			r.log.Sugar().Info("ENV:" + r.config.App.Environment + " Document : " + document.ID + ", discarded: " + err.Error())
			commitMessage(r.log, ctx, r.KafkaReader, m, document.ID, document.TenantID, r.config.App.Environment)
			continue
		}
		if err != nil {
			r.log.Sugar().Info("ENV:" + r.config.App.Environment + " failed to ProcessMessage messages. Document id: " + document.ID + ", error: " + err.Error())
			r.log.Sugar().Error("ENV:" + r.config.App.Environment + " failed to ProcessMessage messages. Document id: " + document.ID + ", error: " + err.Error())
			sentry.CaptureException(errors.New("error processing the document " + document.ID))
			metrics.OpsFailedProcessed.WithLabelValues(r.config.App.Environment, document.TenantID).Inc()
			commitMessage(r.log, ctx, r.KafkaReader, m, document.ID, document.TenantID, r.config.App.Environment)
			continue
		}
		r.log.Sugar().Info("event processed successfully. Document id: " + document.ID)
		metrics.OpsSuccessProcessed.WithLabelValues(r.config.App.Environment, document.TenantID).Inc()
		commitMessage(r.log, ctx, r.KafkaReader, m, document.ID, document.TenantID, r.config.App.Environment)
	}
}

func IsAtleastOneKafkaBrokerHealth(ctx context.Context, brokersAddress []string) bool {
	log := logger.GetLogger()
	for _, broker := range brokersAddress {
		conn, err := kafka.DialContext(ctx, "tcp", broker)
		if err != nil {
			log.Error("error DialContext: ", zap.Error(err))
			continue
		}
		defer conn.Close()
		return true
	}
	return false
}

func IsRightEventVersion(rawData []byte) (bool, error) {
	appVersion, err := getDocumentVersion(rawData)
	if err != nil {
		return false, err
	}
	if appVersion.EventVersion != 1 {
		return false, nil
	}

	return true, nil
}

type DocumentEventVerison struct {
	EventVersion int64 `json:"event_version"`
}

func getDocumentVersion(rawData []byte) (DocumentEventVerison, error) {

	var documentVersion DocumentEventVerison
	if !json.Valid(rawData) {
		return documentVersion, errors.New("invalid JSON syntax")
	}

	err := json.Unmarshal(rawData, &documentVersion)
	if err != nil {

		return documentVersion, errors.New("unable to parse event version")
	}
	return documentVersion, nil
}

func commitMessage(log *zap.Logger, ctx context.Context, KafkaReader *kafka.Reader, m kafka.Message, documentId, tenantId, env string) {
	err := KafkaReader.CommitMessages(ctx, m)
	if err != nil {
		log.Sugar().Info("ENV:" + env + " failed to commit messages. Document id: " + documentId + ", error: " + err.Error())
		log.Sugar().Error("ENV:" + env + " failed to commit messages. Document id: " + documentId + ", error: " + err.Error())
		sentry.CaptureException(errors.New("ENV:" + env + " failed to commit messages "))
		metrics.OpsFailedProcessed.WithLabelValues(env, tenantId).Inc()
	}
}
