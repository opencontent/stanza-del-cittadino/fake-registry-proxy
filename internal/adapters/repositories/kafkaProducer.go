package repositories

import (
	"context"
	"encoding/json"

	"github.com/getsentry/sentry-go"
	"github.com/segmentio/kafka-go"
	"go.uber.org/zap"
	"opencitylabs.it/fakeregistryproxy/config"
	"opencitylabs.it/fakeregistryproxy/internal/core/domain"
	"opencitylabs.it/fakeregistryproxy/internal/core/ports"
	"opencitylabs.it/fakeregistryproxy/logger"
	"opencitylabs.it/fakeregistryproxy/sentryutils"
)

type kafkaProducer struct {
	log         *zap.Logger
	conf        *config.Config
	KafkaWriter *kafka.Writer
	ctx         context.Context
	sentryHub   *sentry.Hub
}

func NewKafkaProducer(ctx context.Context) ports.DocumentProducerRepository {
	r := &kafkaProducer{
		log:       logger.GetLogger(),
		conf:      config.GetConfig(),
		ctx:       ctx,
		sentryHub: sentryutils.InitLocalSentryhub(),
	}
	r.KafkaWriter = r.getKafkaWriter(r.conf.Kafka.KafkaServer, r.conf.Kafka.ProducerTopic, r.conf.Kafka.ConsumerGroup)
	return r
}

func (r *kafkaProducer) ProduceMessage(document domain.Document) error {
	jsonDoc, err := json.Marshal(document)
	if err != nil {
		return err
	}
	keyevent := ""
	if document.RemoteCollection != nil {
		keyevent = document.RemoteCollection.ID
	}
	err = r.KafkaWriter.WriteMessages(r.ctx, kafka.Message{

		Key:   []byte(keyevent),
		Value: jsonDoc,
	})
	if err != nil {
		r.log.Error("could not write message: ", zap.Error(err))

		return err
	}
	return nil
}

func (r *kafkaProducer) ProduceOnRetryQueue(document domain.Document) error {
	kafkaWriter := r.getKafkaWriter(r.conf.Kafka.KafkaServer, r.conf.Kafka.RetryTopic, r.conf.Kafka.ConsumerGroup)

	defer func() {
		err := kafkaWriter.Close()
		if err != nil {
			r.log.Error("document "+document.ID+" Error closing retry producer: ", zap.Error(err))

			return
		}
		r.log.Debug("document " + document.ID + " retry producer closed")
	}()

	byteDoc, err := r.getByteArrayFromStruct(document)
	if err != nil {
		r.log.Error("document "+document.ID+" could not write message: ", zap.Error(err))
		return err
	}

	retryEvent := r.buildMessageWithRetryMetaField(byteDoc)
	if retryEvent == nil {
		r.log.Error("document " + document.ID + " error build event with retry object")
		return err
	}

	keyevent := ""
	if document.RemoteCollection != nil {
		keyevent = document.RemoteCollection.ID
	}
	err = kafkaWriter.WriteMessages(r.ctx, kafka.Message{

		Key:   []byte(keyevent),
		Value: retryEvent,
	})
	if err != nil {
		r.log.Error("could not write message: ", zap.Error(err))
		return err
	}
	r.log.Debug("document " + document.ID + " produced on topic " + r.conf.Kafka.ProducerTopic)
	return nil
}

func (r *kafkaProducer) getByteArrayFromStruct(doc domain.Document) ([]byte, error) {
	jsonData, err := json.Marshal(doc)
	if err != nil {
		return nil, err
	}

	return []byte(jsonData), nil
}
func (r *kafkaProducer) buildMessageWithRetryMetaField(m []byte) []byte {

	// 1 parsare in una mappa
	var message map[string]interface{}
	retryMeta := make(map[string]interface{})

	// Unmarshal the input message into the map
	err := json.Unmarshal(m, &message)
	if err != nil {
		r.log.Debug("error unmarshalling message into map")
		return nil
	}
	if _, ok := message["retry_meta"]; !ok {
		r.log.Debug("we need to add retry_meta fields")
		retryMeta["original_topic"] = r.conf.Kafka.ConsumerTopic
		message["retry_meta"] = retryMeta

		byteMessage, err := json.Marshal(message)
		if err != nil {
			r.log.Debug("error unmarshalling message into []byte")
			return nil
		}

		return byteMessage
	}
	return m
}

func (r *kafkaProducer) getKafkaWriter(kafkaServer []string, kafkaTopic, producerGroup string) *kafka.Writer {
	transport := &kafka.DefaultTransport
	kwriter := &kafka.Writer{
		Addr:      kafka.TCP(kafkaServer...),
		Topic:     kafkaTopic,
		Balancer:  &kafka.LeastBytes{},
		Transport: *transport,
	}
	return kwriter

}

func (r *kafkaProducer) CloseKafkaWriter() {
	err := r.KafkaWriter.Close()
	if err != nil {
		r.log.Error("Error closing producer: ", zap.Error(err))
		sentry.CaptureException(err)
		return
	}
	r.log.Debug("producer closed")
}
