package repositories

import (
	"bufio"
	"context"
	"errors"
	"io"
	"sort"

	"github.com/araddon/gou"
	"github.com/lytics/cloudstorage"
	"github.com/lytics/cloudstorage/awss3"
	"github.com/lytics/cloudstorage/localfs"
	"go.uber.org/zap"
	"google.golang.org/api/iterator"
	"opencitylabs.it/fakeregistryproxy/config"
	"opencitylabs.it/fakeregistryproxy/internal/core/domain"
	"opencitylabs.it/fakeregistryproxy/internal/core/ports"
	"opencitylabs.it/fakeregistryproxy/logger"
	"opencitylabs.it/fakeregistryproxy/utils"
)

type storageRepo struct {
	log   *zap.Logger
	conf  *config.Config
	store cloudstorage.Store
}

func NewStorageRepo() ports.StorageRepository {
	r := &storageRepo{
		log:  logger.GetLogger(),
		conf: config.GetConfig(),
	}
	store, err := r.getStore()
	if err != nil {
		r.log.Error("error getting storage: ", zap.Error(err))
	}
	r.store = store
	return r
}
func (r *storageRepo) getStore() (cloudstorage.Store, error) {
	var config *cloudstorage.Config
	if r.conf.Storage.StorageType == "s3" {
		config = r.getS3Storage()
	} else if r.conf.Storage.StorageType == "local" {
		config = r.getLocalStorage()
	}
	store, err := cloudstorage.NewStore(config)
	if err != nil {
		r.log.Debug("error newStore " + err.Error())
		return store, err
	}
	return store, nil
}

func (r *storageRepo) getS3Storage() *cloudstorage.Config {
	config := &cloudstorage.Config{
		Type:       awss3.StoreType,
		AuthMethod: awss3.AuthAccessKey,
		Endpoint:   r.conf.Storage.EndPoint,
		Region:     r.conf.Storage.StorageRegion,
		Settings: gou.JsonHelper{
			"fake": "notused",
		},
	}
	config.Settings[awss3.ConfKeyAccessKey] = r.conf.Storage.AccessKey
	config.Settings[awss3.ConfKeyAccessSecret] = r.conf.Storage.KeyAccessSecret
	config.Bucket = r.conf.Storage.Bucket
	config.TmpDir = "/tmp/localcache/aws"

	r.log.Debug("created s3 cloud config...")

	return config
}

func (r *storageRepo) getLocalStorage() *cloudstorage.Config {
	config := &cloudstorage.Config{
		Type:       localfs.StoreType,
		AuthMethod: localfs.AuthFileSystem,
		LocalFS:    "configuration/data",
		TmpDir:     "configuration/data/tmp",
	}

	r.log.Debug("created local storage config...")

	return config
}

// tenants
func (r *storageRepo) GetTenantConfigList(offset, limit int) ([]domain.TenantConfig, error) {
	var allTenants []domain.TenantConfig

	q := cloudstorage.NewQuery(r.conf.Storage.StorageBasePath + "tenants/")
	iter, err := r.store.Objects(context.Background(), q)
	if err != nil {
		r.log.Error("error objects ", zap.Error(err))
		return allTenants, err
	}
	for {
		o, err := iter.Next()
		if err == iterator.Done {
			break
		}
		obj2, _ := r.store.Get(context.Background(), o.Name())
		f2, _ := obj2.Open(cloudstorage.ReadOnly)
		defer func() {
			err := obj2.Close()
			if err != nil {
				r.log.Error("error closing file: %v", zap.Error(err))
			}
		}()
		bytes, _ := io.ReadAll(f2)
		if len(bytes) > 0 {
			tenantconfig, err := utils.GetTenantConfigStruct(bytes)
			if err != nil {
				r.log.Error("error getting tenantConfig Struct ", zap.Error(err))
				return allTenants, err
			}
			allTenants = append(allTenants, tenantconfig)
		} else {
			r.log.Debug("empty tenant config list")
		}

	}
	// Apply pagination to the result
	startIndex := offset
	endIndex := offset + limit
	if startIndex < 0 {
		startIndex = 0
	}
	if endIndex > len(allTenants) {
		endIndex = len(allTenants)
	}
	sort.SliceStable(allTenants, func(i, j int) bool {
		return allTenants[i].CreatedAt < allTenants[j].CreatedAt
	})

	return allTenants[startIndex:endIndex], nil
}

func (r *storageRepo) GetTenantById(tenantId string) (domain.TenantConfig, error) {
	var tenantConfig domain.TenantConfig
	q := cloudstorage.NewQuery(r.conf.Storage.StorageBasePath + "tenants/")
	iter, err := r.store.Objects(context.Background(), q)
	if err != nil {
		r.log.Error("error objects ", zap.Error(err))
		return tenantConfig, err
	}
	for {
		o, err := iter.Next()
		if err == iterator.Done {
			break
		}
		if o != nil {
		} else {
			r.log.Debug("object empty!")
		}
		obj2, _ := r.store.Get(context.Background(), o.Name())
		f2, _ := obj2.Open(cloudstorage.ReadOnly)
		bytes, _ := io.ReadAll(f2)
		if len(bytes) > 0 {
			tenantconfig, err := utils.GetTenantConfigStruct(bytes)
			if err != nil {
				r.log.Error("error getting tenantConfig Struct ", zap.Error(err))
				return tenantConfig, err
			}
			if tenantconfig.ID == tenantId {
				return tenantconfig, nil
			}
		}

	}
	return tenantConfig, nil
}

func (r *storageRepo) StoreTenantConfig(tenant domain.TenantConfig) error {

	obj, err := r.store.NewObject(r.conf.Storage.StorageBasePath + "tenants/" + tenant.ID)
	// Close sync's the local file to the remote store and removes the local tmp file.

	if err != nil {
		//object not found
		return err
	}
	defer func() {
		err := obj.Close()
		if err != nil {
			r.log.Error("error closing file: %v", zap.Error(err))
		}
	}()
	// open for read and writing.  f is a filehandle to the local filesystem.
	f, err := obj.Open(cloudstorage.ReadWrite)
	if err != nil {
		r.log.Error("error opening cloud storage file ", zap.Error(err))
		return err
	}

	w := bufio.NewWriter(f)
	jsonStr, err := utils.GetJSONTenantConfigFromStruct(tenant)
	if err != nil {
		r.log.Error("error retrieving json from struct ", zap.Error(err))
		return err
	}
	w.WriteString(jsonStr)
	w.Flush()

	return nil
}

func (r *storageRepo) RemoveTenantConfig(tenantId string) error {
	found := false
	q := cloudstorage.NewQuery(r.conf.Storage.StorageBasePath + "tenants/")

	// Create an Iterator
	iter, err := r.store.Objects(context.Background(), q)
	if err != nil {
		r.log.Error("error retrieving iterator from storage ", zap.Error(err))
		return err
	}
	for {
		o, err := iter.Next()
		if err == iterator.Done {
			break
		}
		if o.Name() == r.conf.Storage.StorageBasePath+"tenants/"+tenantId {
			err = r.store.Delete(context.Background(), o.Name())
			found = true
			if err == iterator.Done {
				r.log.Error("error deleting file ", zap.Error(err))
				break

			}
		}
	}
	if !found {
		return errors.New("object not found")
	}
	return err
}

func (r *storageRepo) UpdateTenantConfig(tenant domain.TenantConfig) (domain.TenantConfig, error) {
	tenantConfig, err := r.GetTenantById(tenant.ID)
	if err != nil {
		r.log.Error("error retrieving tenantConfig with id: "+tenant.ID+" ", zap.Error(err))
		return tenantConfig, err
	}

	tenant.CreatedAt = tenantConfig.CreatedAt

	if tenantConfig.ID == "" {
		r.log.Debug("error updating tenantConfig: object with id: " + tenant.ID + " not found  ")
		return tenantConfig, errors.New(" object with id " + tenant.ID + " not found  ")
	}
	err = r.RemoveTenantConfig(tenant.ID)
	if err != nil {
		r.log.Error("error updating tenantConfig  ", zap.Error(err))
		return tenantConfig, err
	}
	err = r.StoreTenantConfig(tenant)
	if err != nil {
		r.log.Error("error updating tenantConfig  ", zap.Error(err))
		return tenantConfig, err
	}

	return tenantConfig, nil
}

// services
func (r *storageRepo) GetServiceConfigList() ([]domain.ServiceConfig, error) {
	var serviceConfigSlice []domain.ServiceConfig
	q := cloudstorage.NewQuery(r.conf.Storage.StorageBasePath + "services/")
	iter, err := r.store.Objects(context.Background(), q)
	if err != nil {
		r.log.Error("error objects ", zap.Error(err))
		return serviceConfigSlice, err
	}
	for {
		o, err := iter.Next()
		if err == iterator.Done {
			break
		}
		obj2, _ := r.store.Get(context.Background(), o.Name())
		f2, _ := obj2.Open(cloudstorage.ReadOnly)
		bytes, _ := io.ReadAll(f2)
		if len(bytes) > 0 {
			serviceConfig, err := utils.GetServiceConfigStruct(bytes)
			if err != nil {
				r.log.Error("error getting serviceConfig Struct ", zap.Error(err))
				return serviceConfigSlice, err
			}
			serviceConfigSlice = append(serviceConfigSlice, serviceConfig)
		} else {
			r.log.Debug("empty tenant config list")
		}

	}
	return serviceConfigSlice, nil
}
func (r *storageRepo) GetServiceById(serviceId string) (domain.ServiceConfig, error) {
	var serviceConfig domain.ServiceConfig

	q := cloudstorage.NewQuery(r.conf.Storage.StorageBasePath + "services/")
	iter, err := r.store.Objects(context.Background(), q)
	if err != nil {
		r.log.Error("error objects ", zap.Error(err))
		return serviceConfig, err
	}
	for {
		o, err := iter.Next()
		if err == iterator.Done {
			break
		}
		obj2, _ := r.store.Get(context.Background(), o.Name())
		f2, _ := obj2.Open(cloudstorage.ReadOnly)
		bytes, _ := io.ReadAll(f2)
		if len(bytes) > 0 {
			serviceconfig, err := utils.GetServiceConfigStruct(bytes)
			if err != nil {
				r.log.Error("error getting serviceConfig Struct ", zap.Error(err))
				return serviceConfig, err
			}
			if serviceconfig.ID == serviceId {
				return serviceconfig, nil
			}
		}

	}
	return serviceConfig, nil
}
func (r *storageRepo) StoreServiceConfig(service domain.ServiceConfig) error {
	r.log.Debug("StoreServiceConfig 1 " + r.conf.Storage.StorageBasePath + "services/" + service.ID)
	obj, err := r.store.NewObject(r.conf.Storage.StorageBasePath + "services/" + service.ID)

	if err != nil {
		r.log.Debug("StoreServiceConfig 2 : " + err.Error())
		return err
	}
	// Close sync's the local file to the remote store and removes the local tmp file.
	defer func() {
		err := obj.Close()
		if err != nil {
			r.log.Error("error closing file: %v", zap.Error(err))
		}
	}()
	// open for read and writing.  f is a filehandle to the local filesystem.
	f, err := obj.Open(cloudstorage.ReadWrite)
	if err != nil {
		r.log.Debug("StoreServiceConfig 3")
		r.log.Error("error opening cloud storage file ", zap.Error(err))
		return err
	}
	r.log.Debug("StoreServiceConfig 5")
	w := bufio.NewWriter(f)
	jsonStr, err := utils.GetJSONServiceConfigFromStruct(service)
	if err != nil {
		r.log.Debug("StoreServiceConfig 6")
		r.log.Error("error retrieving json from struct ", zap.Error(err))
		return err
	}
	r.log.Debug("StoreServiceConfig 7")
	w.WriteString(jsonStr)
	w.Flush()

	return nil
}
func (r *storageRepo) RemoveServiceConfig(serviceId string) error {
	found := false
	q := cloudstorage.NewQuery(r.conf.Storage.StorageBasePath + "services/")
	// Create an Iterator
	iter, err := r.store.Objects(context.Background(), q)
	if err != nil {
		r.log.Error("error retrieving iterator from storage ", zap.Error(err))
		return err
	}
	for {
		o, err := iter.Next()
		if err == iterator.Done {
			break
		}
		if o.Name() == r.conf.Storage.StorageBasePath+"services/"+serviceId {
			err = r.store.Delete(context.Background(), o.Name())
			found = true
			if err == iterator.Done {
				r.log.Error("error deleting file ", zap.Error(err))
				break
			}
		}
	}
	if !found {
		return errors.New("object not found")
	}
	return nil
}
func (r *storageRepo) UpdateServiceConfig(service domain.ServiceConfig) (domain.ServiceConfig, error) {
	serviceConfig, err := r.GetServiceById(service.ID)
	if err != nil {
		r.log.Error("error retrieving serviceConfig with id: "+service.ID+" ", zap.Error(err))
		return serviceConfig, err
	}
	if serviceConfig.ID == "" {
		r.log.Debug("error updating serviceConfig: object with id: " + service.ID + " not found  ")
		return serviceConfig, errors.New(" object with id " + service.ID + " not found  ")
	}

	err = r.RemoveServiceConfig(service.ID)
	if err != nil {
		r.log.Debug("error updating serviceConfig  ", zap.Error(err))
		return serviceConfig, err
	}

	err = r.StoreServiceConfig(service)
	if err != nil {
		r.log.Debug("error updating serviceConfig  ", zap.Error(err))
		return serviceConfig, err
	}

	return serviceConfig, nil
}
