package repositories

import (
	"go.uber.org/zap"
	"opencitylabs.it/fakeregistryproxy/config"
	"opencitylabs.it/fakeregistryproxy/internal/core/domain"
	"opencitylabs.it/fakeregistryproxy/internal/core/ports"
	"opencitylabs.it/fakeregistryproxy/logger"
)

type fakeProtocolSystem struct {
	log  *zap.Logger
	conf *config.Config
}

func NewFakeProtocolSystem() ports.ProtocolSystem {
	r := &fakeProtocolSystem{
		log:  logger.GetLogger(),
		conf: config.GetConfig(),
	}
	return r
}

func (r *fakeProtocolSystem) GenerateProtocolDocument(document domain.Document) (string, error) {
	return "123456", nil
}
