# Fake Registry Proxy 

## Contesto

Questo componente è parte del sistema di generazione dei documenti a partire dalle relative pratiche per permettere la protocollazione in maniera indipendente.  

## Cosa fa 

Il fake registry proxy legge dal topic documents, e per ogni documento simulerà la relativa protocollazione.
Inoltre, il servizio espone le configurazione per i tenant, i servizi e lo schema utilizzati dalla stanza per abilitare tale proxy di protocollazione.

## Struttura del repository
Il repository ha come unico riferimento il branch main. \
La struttura di base riprende i principi del hexagonal architecture in cui:\
**main.go** - entrypoint del servizio. Qui vengono assemblate e avviate le strutture seguenti.\
**intenrnal/core/domain**: - contiene le entità utilizzate dalla business logic\
**internal/core/services**: - contiente la business logic\
**internal/core/ports**: - contiente le intefacce che descrivono come i driver e i driven interagiscono la busines logic\
**internal/adapters/drivers**: - contiene i driver, ovvero le tecnologie esposte dal servizio per poter essere richiamati dall'esterno \
**internal/adapters/repositories**: - contiene i driven, ovvero le tecnologie utilizzate dal servizio per poter accedere al mondo esterno \
**config/** - contiene la struttura atta alla configurazione del servizio: variabili d'ambiente, configurazione hardcoded\
**metrics/** - contiene la descrizione delle metriche Prometheus esposte dal servizio\
**sentryUtils/** - contiene la configurazione di Sentry per il monitoring degli errori\
**logger/** - contiene la configurazione del sistema di logs in formato in formato Apache\
**utils/** - contiene le utils del codice(funzioni accessorie..) \
Altri File: \
file di configurazione per la continuous integration:\ 
dockerFile, docker-compose.yml, docker-compose.ovverride.yml, gitlab-ci.yml, dockerignore, gitignore\

## Prerequisiti e dipendenze
tutte le librerie esterne utilizzate sono elencate nel file go.mod. La sola presenza di tale file e del suo attuale contenuto permette una gestione automatica delle dipendenze. \
È necessaria l'installazione di Docker e Docker Compose per il deploy automatico del servizio e di tutti gli applicativi accessori(kafka, kafka-ui, ksqldb-server, ksqldb-cli, zookeeper..) 

## Monitoring
il sistema usufruisce di Sentry per il monitoring degli errori, inoltre sono esposti i seguenti endpoint per il monitoring : \
- /status : endpoint per l'healtcheck. Resituisce 200
- /metrics : espone le metriche in formato Prometheus

### Configurazione variabili d'ambiente
| Nome                             | Default   | Descrizione                                                             |
|----------------------------------|-----------|-------------------------------------------------------------------------|
| ENVIRONMENT                      | local     | Indica l'ambiente di sviluppo (locale, dev, prod, ecc.) utilizzato da Sentry. |
| KAFKA_SERVER                     | kafka:9092| Indirizzo del broker Kafka per connettersi al cluster.                  |
| KAFKA_CONSUMER_GROUP             | fakeregistryproxy | Consumer group per Kafka.                                  |
| KAFKA_CONSUMER_TOPIC             | documents | Identifica il topic da cui consumare gli eventi Kafka.                  |
| KAFKA_PRODUCER_TOPIC             | documents | Identifica il topic a cui inviare gli eventi Kafka.                     |
| SERVER_ADDRESS_PORT              | 0.0.0.0:9090 | Indica l'indirizzo e la porta utilizzati per l'healthcheck.         |
| STORAGE_ENDPOINT                 | http://minio:9000 | Indirizzo di accesso allo storage.                           |
| STORAGE_ACCESS_KEY               | AKIAIOSFODNN7EXAMPLE | Chiave di accesso allo storage.                    |
| STORAGE_KEY_ACCESS_SECRET        | wJalrXUtnFEMI/K7MDENG/bPxRfiCYEXAMPLEKEY | Chiave segreta di accesso allo storage.  |
| STORAGE_BASE_PATH                   | /opencity-bucket/ | Bucket di storage.                                               |
| STORAGE_BASE_PATH               | fake/ | base path dello storage                    |
| STORAGE_S3_REGION                |  eu-west-1              | location del cloud storage  |
|STORAGE_TYPE |  local              | type of storage (local/s3)|
| SENTRY_DSN                       | default | Endpoint per il monitoraggio di Sentry. |


## Come si usa
Aggiungere il file docker-compose.override.yml avente seguente struttura : 
```yaml
version: '<la tua versione>'
networks:
  core_default:
    external: true

services:
  fakeregistryproxy:
    build:
      context: '<il tuo contesto>'
    environment:
        ENVIRONMENT: '<il tuo ambiente>'
        KAFKA_SERVER: '<il tuo server Kafka>'
        KAFKA_CONSUMER_GROUP: '<il tuo gruppo di consumatori Kafka>'
        KAFKA_CONSUMER_TOPIC: '<il tuo topic di consumatori Kafka>'
        KAFKA_PRODUCER_TOPIC: '<il tuo topic di produttori Kafka>'
        SERVER_ADDRESS_PORT: '<il tuo indirizzo e porta del server>'
        
        STORAGE_ENDPOINT: '<il tuo endpoint di archiviazione>'
        STORAGE_ACCESS_KEY: '<la tua chiave di accesso a Storage>'
        STORAGE_KEY_ACCESS_SECRET: '<il tuo segreto di accesso a Storage>'
        STORAGE_BUCKET: '<il tuo bucket di archiviazione>'
        STORAGE_SCHEMA_REPO: '<il tuo repository degli schemi di archiviazione>'
        STORAGE_TENANTS_REPO: '<il tuo repository dei tenant di archiviazione>'
        STORAGE_SERVICE_REPO: '<il tuo repository dei servizi di archiviazione>'

        SENTRY_DSN: '<il tuo SENTRY_DSN>'
    ports:
      - "<la tua porta esterna>:9090"
    volumes:
      - '<il tuo percorso al file JSON dei tenant>:/mocks/tenantsList.json'
    networks:
      - core_default
      - default
```

Da terminale: 
1. `git clone  git@gitlab.com:opencity-labs/area-personale/fake-registry-proxy.git`
2. `cd fake-registry-proxy`
3. `docker-compose build`
4. `docker compose up -d`

## Swagger
le API sono documente e consultabili all'indirizzo: 

base url + /swagger/index.html


## Testing
sono presenti unit test. per lanciarli, utilizzare il commando `go test .`

## Stadio di sviluppo

il software si trova in fase `Feature-complete`

## Autori e Riconoscimenti

* Opencity Labs

## License

AGPL 3.0 only

## Contatti

support@opencitylabs.it

