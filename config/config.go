package config

import (
	"os"
	"strconv"
	"strings"
	"sync"

	"opencitylabs.it/fakeregistryproxy/logger"
)

type Config struct {
	App struct {
		AppVersion  string
		Environment string
	}
	Kafka struct {
		KafkaServer   []string
		ConsumerGroup string
		ConsumerTopic string
		ProducerTopic string
		RetryTopic    string
	}
	Server struct {
		AddressPort         string
		HealthCheckEndPoint string
		Metrics             string
		DebugMode           bool
	}
	Sentry struct {
		Dsn string
	}
	Storage struct {
		EndPoint        string
		AccessKey       string
		KeyAccessSecret string
		Bucket          string
		StorageRegion   string
		StorageBasePath string
		StorageType     string
	}
}

var lock = &sync.Mutex{}
var appConfig *Config

func GetConfig() *Config {
	if appConfig != nil {
		return appConfig
	}
	lock.Lock()
	defer lock.Unlock()

	//re-check after locking
	if appConfig != nil {
		return appConfig
	}
	appConfig = initConfig()
	return appConfig
}

func initConfig() *Config {
	logger := logger.GetLogger()
	logger.Debug("loading env vars....")
	var c Config
	c.App.AppVersion = VERSION
	c.App.Environment = getFromEnv("ENVIRONMENT", "test")
	kafkaServer := getFromEnv("KAFKA_SERVER", "redpanda-1:29092,redpanda-2:29092,redpanda-3:29092")
	c.Kafka.KafkaServer = strings.Split(kafkaServer, ",")
	c.Kafka.ConsumerGroup = getFromEnv("KAFKA_CONSUMER_GROUP", "test")
	c.Kafka.ConsumerTopic = getFromEnv("KAFKA_CONSUMER_TOPIC", "test")
	c.Kafka.ProducerTopic = getFromEnv("KAFKA_PRODUCER_TOPIC", "test")
	c.Kafka.RetryTopic = getFromEnv("KAFKA_RETRY_TOPIC", "test")
	c.Server.AddressPort = getFromEnv("SERVER_ADDRESS_PORT", "0.0.0.0:9090")
	c.Server.DebugMode = getBoolFromEnv("SERVER_DEBUG_MODE", false)
	c.Storage.EndPoint = getFromEnv("STORAGE_ENDPOINT", "http://minio:9000")
	c.Storage.AccessKey = getFromEnv("STORAGE_ACCESS_KEY", "AKIAIOSFODNN7EXAMPLE")
	c.Storage.KeyAccessSecret = getFromEnv("STORAGE_KEY_ACCESS_SECRET", "wJalrXUtnFEMI/K7MDENG/bPxRfiCYEXAMPLEKEY")
	c.Storage.Bucket = getFromEnv("STORAGE_BUCKET", "/opencity-bucket/")
	c.Storage.StorageRegion = getFromEnv("STORAGE_S3_REGION", "services/")
	c.Storage.StorageBasePath = getFromEnv("STORAGE_BASE_PATH", "fake")
	c.Storage.StorageType = getFromEnv("STORAGE_TYPE", "local")

	c.Sentry.Dsn = getFromEnv("SENTRY_DSN", "test")
	return &c
}

func getFromEnv(name string, defaultValue string) string {
	logger := logger.GetLogger()
	if val, ok := os.LookupEnv(name); ok {
		return val
	}
	logger.Sugar().Debug("using default value for environment var: ", name)
	return defaultValue
}

func getBoolFromEnv(name string, defaultValue bool) bool {
	logger := logger.GetLogger()

	if val, ok := os.LookupEnv(name); ok {
		// Attempt to parse the environment variable as a boolean
		boolValue, err := strconv.ParseBool(val)
		if err != nil {
			// Log an error if parsing fails and return the default value
			logger.Sugar().Errorf("Error parsing boolean value for environment var %s: %v", name, err)
			return defaultValue
		}
		return boolValue
	}

	logger.Sugar().Debug("Using default value for environment var: ", name)
	return defaultValue
}
