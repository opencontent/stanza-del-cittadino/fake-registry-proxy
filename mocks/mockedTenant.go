package mocks

import (
	"encoding/json"
	"fmt"
)

type TenantsList struct {
	Count    int      `json:"count"`
	Next     string   `json:"next"`
	Previous string   `json:"previous"`
	Results  []Tenant `json:"results"`
}

type Tenant struct {
	ID                               int    `json:"id"`
	Description                      string `json:"description"`
	Slug                             string `json:"slug"`
	SdcID                            string `json:"id"`
	SdcBaseURL                       string `json:"base_url"`
	SdcUsername                      string `json:"username"`
	SdcPassword                      string `json:"password"`
	SdcInstitutionCode               string `json:"institution_code"`
	SdcAooCode                       string `json:"aoo_code"`
	LatestRegistrationNumber         int64  `json:"latest_registration_number"`
	LatestRegistrationNumberIssuedAt string `json:"latest_registration_number_issued_at"`
	RegisterAfterDate                string `json:"register_after_date"`
	CreatedAt                        string `json:"created_at"`
	ModifiedAt                       string `json:"modified_at"`
}

var TenantList = TenantsList{}

/* func TenantsDbInit() {
	r := rand.New(rand.NewSource(time.Now().UnixNano()))

	// Generate random values for a single Tenant
	randomTenant := Tenant{
		ID:                               r.Intn(1000),
		Description:                      "Random Tenant Description",
		Slug:                             "random-tenant-slug",
		SdcID:                            "random-sdc-id",
		SdcBaseURL:                       "https://sdc.example.com",
		SdcUsername:                      "random-username",
		SdcPassword:                      "random-password",
		SdcInstitutionCode:               "random-institution-code",
		SdcAooCode:                       "random-aoo-code",
		LatestRegistrationNumber:         r.Int63n(1000000),
		LatestRegistrationNumberIssuedAt: time.Now().Add(-time.Duration(r.Intn(365)) * 24 * time.Hour).Format("2006-01-02"),
		RegisterAfterDate:                time.Now().Add(time.Duration(r.Intn(365)) * 24 * time.Hour).Format("2006-01-02"),
		CreatedAt:                        time.Now().Add(-time.Duration(r.Intn(365)) * 24 * time.Hour).Format("2006-01-02"),
		ModifiedAt:                       time.Now().Format("2006-01-02"),
	}

	numberOfTenants := 5
	TenantList = TenantsList{
		Count:    numberOfTenants,
		Next:     "https://example.com/api/tenants?page=2",
		Previous: "",
		Results:  make([]Tenant, numberOfTenants),
	}

	for i := 0; i < numberOfTenants; i++ {
		// Modify randomTenant to create a new random tenant and append to the list
		randomTenant.ID = r.Intn(1000)
		randomTenant.Description = fmt.Sprintf("Random Tenant Description %d", i)
		TenantList.Results[i] = randomTenant
	}
} */

func TenantsDbInit() {
	TenantList, _ = TenantListJsonToStruct(TENANTS_LIST_MOCK)
}

func TenantListJson() string {
	jsonData, err := json.MarshalIndent(TenantList, "", "    ")
	if err != nil {
		fmt.Println("Error:", err)
		return ""
	}

	return string(jsonData)
}

func TenantJson(tenant Tenant) string {
	jsonData, err := json.MarshalIndent(tenant, "", "    ")
	if err != nil {
		fmt.Println("Error:", err)
		return ""
	}

	return string(jsonData)
}

func TenantJsonToStruct(jsonData string) (Tenant, error) {
	var tenant Tenant

	err := json.Unmarshal([]byte(jsonData), &tenant)
	if err != nil {
		fmt.Println("Error:", err)
		return tenant, err
	}

	return tenant, nil
}

func TenantListJsonToStruct(jsonData string) (TenantsList, error) {
	var tenant TenantsList

	err := json.Unmarshal([]byte(jsonData), &tenant)
	if err != nil {
		fmt.Println("Error:", err)
		return tenant, err
	}

	return tenant, nil
}

func GetTenantByid(id int) string {
	for _, tenant := range TenantList.Results {
		if tenant.ID == id {
			return TenantJson(tenant)
		}
	}
	return ""
}

func TenantUpdate(id int, tenantUpdate Tenant) {
	for i := range TenantList.Results {
		if TenantList.Results[i].ID == id {
			TenantList.Results[i].Description = tenantUpdate.Description
			TenantList.Results[i].Slug = tenantUpdate.Slug
			TenantList.Results[i].SdcID = tenantUpdate.SdcID
			TenantList.Results[i].SdcBaseURL = tenantUpdate.SdcBaseURL
			TenantList.Results[i].SdcUsername = tenantUpdate.SdcUsername
			TenantList.Results[i].SdcPassword = tenantUpdate.SdcPassword
			TenantList.Results[i].SdcInstitutionCode = tenantUpdate.SdcInstitutionCode
			TenantList.Results[i].SdcAooCode = tenantUpdate.SdcAooCode
			TenantList.Results[i].Description = tenantUpdate.Description
			TenantList.Results[i].LatestRegistrationNumber = tenantUpdate.LatestRegistrationNumber
			TenantList.Results[i].LatestRegistrationNumberIssuedAt = tenantUpdate.LatestRegistrationNumberIssuedAt
			TenantList.Results[i].RegisterAfterDate = tenantUpdate.RegisterAfterDate
			TenantList.Results[i].CreatedAt = tenantUpdate.CreatedAt
			TenantList.Results[i].ModifiedAt = tenantUpdate.ModifiedAt
			return
		}
	}
}

func TenantDestroy(id int) {
	for i, tenant := range TenantList.Results {
		if tenant.ID == id {
			TenantList.Results = append(TenantList.Results[:i], TenantList.Results[i+1:]...)
			return
		}
	}
}
