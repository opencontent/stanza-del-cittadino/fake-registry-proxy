package mocks

const FORM_SCHEMA_MOCK = `{
  "display": "form",
  "settings": null,
  "components": [
    {
      "label": "UUID dell'ente",
      "placeholder": "XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX",
      "applyMaskOn": "change",
      "hidden": true,
      "tableView": true,
      "clearOnHide": false,
      "validate": {
        "required": true
      },
      "key": "tenant_id",
      "type": "textfield",
      "input": true
    },
    {
      "label": "UUID del Servizio",
      "placeholder": "XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX",
      "applyMaskOn": "change",
      "hidden": true,
      "tableView": true,
      "clearOnHide": false,
      "validate": {
        "required": true
      },
      "key": "id",
      "type": "textfield",
      "input": true
    },
    {
      "label": "Descrizione del servizio",
      "placeholder": "Comune di Bugliano",
      "applyMaskOn": "change",
      "hidden": true,
      "tableView": true,
      "defaultValue": " ",
      "clearOnHide": false,
      "validate": {
        "required": true
      },
      "key": "description",
      "type": "textfield",
      "input": true
    },
    {
      "label": "Impostazioni specifiche del protocollo",
      "tableView": false,
      "key": "registry",
      "type": "container",
      "input": true,
      "components": [
        {
          "label": "URL Protocollazione",
          "placeholder": "http://www.example.com/",
          "tableView": true,
          "validate": {
            "required": true
          },
          "key": "url",
          "type": "textfield",
          "input": true
        },
        {
          "label": "Gerarchia di Classificazione",
          "placeholder": "1.2.3",
          "tableView": true,
          "clearOnHide": false,
          "key": "classification_hierarchy",
          "type": "textfield",
          "input": true
        },
        {
          "label": "Numero Fascicolo",
          "placeholder": "1",
          "tableView": true,
          "clearOnHide": false,
          "key": "folder_number",
          "type": "textfield",
          "input": true
        },
        {
          "label": "Anno Fascicolo",
          "placeholder": "2022",
          "tableView": true,
          "clearOnHide": false,
          "key": "folder_year",
          "type": "textfield",
          "input": true
        }
      ]
    },
    {
      "label": "Attivo",
      "defaultValue": false,
      "clearOnHide": false,
      "key": "is_active",
      "type": "checkbox",
      "input": true,
      "tableView": false
    },
    {
      "label": "Salva",
      "showValidations": false,
      "disableOnInvalid": true,
      "tableView": false,
      "key": "submit",
      "type": "button",
      "saveOnEnter": false,
      "input": true
    }
  ]
}

`

const TENANTS_LIST_MOCK = `{
    "count": 5,
    "next": "https://example.com/api/tenants?page=2",
    "previous": "",
    "results": [
      {
        "id": 106,
        "description": "Random Tenant Description 0",
        "slug": "random-tenant-slug",
        "id": "random-sdc-id",
        "base_url": "https://sdc.example.com",
        "username": "random-username",
        "password": "random-password",
        "institution_code": "random-institution-code",
        "aoo_code": "random-aoo-code",
        "latest_registration_number": 943517,
        "latest_registration_number_issued_at": "2023-04-18",
        "register_after_date": "2024-01-13",
        "created_at": "2023-07-05",
        "modified_at": "2023-08-07"
      },
      {
        "id": 780,
        "description": "Random Tenant Description 1",
        "slug": "random-tenant-slug",
        "id": "random-sdc-id",
        "base_url": "https://sdc.example.com",
        "username": "random-username",
        "password": "random-password",
        "institution_code": "random-institution-code",
        "aoo_code": "random-aoo-code",
        "latest_registration_number": 943517,
        "latest_registration_number_issued_at": "2023-04-18",
        "register_after_date": "2024-01-13",
        "created_at": "2023-07-05",
        "modified_at": "2023-08-07"
      },
      {
        "id": 160,
        "description": "Random Tenant Description 2",
        "slug": "random-tenant-slug",
        "id": "random-sdc-id",
        "base_url": "https://sdc.example.com",
        "username": "random-username",
        "password": "random-password",
        "institution_code": "random-institution-code",
        "aoo_code": "random-aoo-code",
        "latest_registration_number": 943517,
        "latest_registration_number_issued_at": "2023-04-18",
        "register_after_date": "2024-01-13",
        "created_at": "2023-07-05",
        "modified_at": "2023-08-07"
      },
      {
        "id": 458,
        "description": "Random Tenant Description 3",
        "slug": "random-tenant-slug",
        "id": "random-sdc-id",
        "base_url": "https://sdc.example.com",
        "username": "random-username",
        "password": "random-password",
        "institution_code": "random-institution-code",
        "aoo_code": "random-aoo-code",
        "latest_registration_number": 943517,
        "latest_registration_number_issued_at": "2023-04-18",
        "register_after_date": "2024-01-13",
        "created_at": "2023-07-05",
        "modified_at": "2023-08-07"
      },
      {
        "id": 486,
        "description": "Random Tenant Description 4",
        "slug": "random-tenant-slug",
        "id": "random-sdc-id",
        "base_url": "https://sdc.example.com",
        "username": "random-username",
        "password": "random-password",
        "institution_code": "random-institution-code",
        "aoo_code": "random-aoo-code",
        "latest_registration_number": 943517,
        "latest_registration_number_issued_at": "2023-04-18",
        "register_after_date": "2024-01-13",
        "created_at": "2023-07-05",
        "modified_at": "2023-08-07"
      }
    ]
  }
`
