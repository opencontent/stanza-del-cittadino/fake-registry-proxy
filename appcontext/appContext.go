package appcontext

import (
	"context"
	"sync"

	"opencitylabs.it/fakeregistryproxy/config"
)

type AppContext struct {
	ctx              context.Context
	exitFn           context.CancelFunc
	wg               *sync.WaitGroup
	databaseSync     *sync.RWMutex
	cloudStorageSync *sync.RWMutex
	conf             *config.Config
}

func (r *AppContext) InitContext() {
	r.wg = &sync.WaitGroup{}
	r.databaseSync = &sync.RWMutex{}
	r.cloudStorageSync = &sync.RWMutex{}
	r.ctx, r.exitFn = context.WithCancel(context.Background())
	r.conf = config.GetConfig()
}

func (r *AppContext) GetContext() context.Context {
	return r.ctx
}
func (r *AppContext) GetContextExitFn() context.CancelFunc {
	return r.exitFn
}

func (r *AppContext) GetDatabaseSync() *sync.RWMutex {
	return r.databaseSync
}

func (r *AppContext) GetCloudStorageSync() *sync.RWMutex {
	return r.cloudStorageSync
}

func (r *AppContext) GetConfig() *config.Config {
	return r.conf
}
