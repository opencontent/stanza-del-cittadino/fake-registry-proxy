package main

import (
	"context"
	"os"
	"os/signal"
	"sync"
	"syscall"
	"time"

	"github.com/getsentry/sentry-go"
	"go.uber.org/zap"
	"opencitylabs.it/fakeregistryproxy/appcontext"
	"opencitylabs.it/fakeregistryproxy/config"
	"opencitylabs.it/fakeregistryproxy/internal/adapters/drivers"
	"opencitylabs.it/fakeregistryproxy/internal/adapters/repositories"
	"opencitylabs.it/fakeregistryproxy/internal/core/services"
	"opencitylabs.it/fakeregistryproxy/logger"
	"opencitylabs.it/fakeregistryproxy/sentryutils"
)

func main() {

	appContext := appcontext.AppContext{}
	appContext.InitContext()

	config := config.GetConfig()
	logger := logger.GetLogger()
	ctx, exitFn := context.WithCancel(context.Background())
	wg := &sync.WaitGroup{}

	err := sentryutils.SentryInit(config.Sentry.Dsn, config.App.AppVersion, config.App.Environment)
	if err != nil {
		logger.Error("sentry.Init: ", zap.Error(err))
		sentry.CaptureException(err)
	}

	//repositories init
	kafkaProducerRepo := repositories.NewKafkaProducer(ctx)
	protocolSystemRepo := repositories.NewFakeProtocolSystem()
	storage := repositories.NewStorageRepo()
	storageProxy := repositories.NewStorageRepo()

	//service init
	protocollerService := services.NewProtocollerService(appContext, kafkaProducerRepo, protocolSystemRepo, storage)
	confManager := services.NewConfigManagerService(appContext, storageProxy)

	//drivers init
	server := drivers.NewRegistryProxyServer(confManager, ctx)
	kafkaConsumerDriver := drivers.NewKafkaConsumer(config.Kafka.KafkaServer, config.Kafka.ConsumerTopic, config.Kafka.ConsumerGroup, protocollerService)

	wg.Add(2)
	go func() {
		defer wg.Done()
		logger.Debug("server listening on " + config.Server.AddressPort)
		server.Run()
	}()

	go func() {
		defer wg.Done()
		kafkaConsumerDriver.ProcessMessage(ctx)
	}()

	// gracefully shutdown
	quit := make(chan os.Signal, 1)

	signal.Notify(quit, syscall.SIGINT, syscall.SIGTERM)
	<-quit

	wg.Wait()
	_ = logger.Sync()
	sentry.Flush(2 * time.Second)
	exitFn()
	kafkaConsumerDriver.CloseKafkaReader()
	kafkaProducerRepo.CloseKafkaWriter()

	// a timeout of 15 seconds to shutdown the server
	ctx, cancel := context.WithTimeout(context.Background(), 15*time.Second)
	defer cancel()

	logger.Debug("/n all processes done, shutting down")
}
