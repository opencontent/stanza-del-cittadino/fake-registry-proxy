package utils

import (
	"encoding/json"
	"errors"
	"time"

	"opencitylabs.it/fakeregistryproxy/internal/core/domain"
)

func GetDocumentStruct(rawData []byte) (domain.Document, error) {
	var documentMessage domain.Document
	if !json.Valid(rawData) {
		return documentMessage, errors.New("invalid JSON syntax")
	}

	if err := json.Unmarshal(rawData, &documentMessage); err != nil {
		return documentMessage, err
	}
	return documentMessage, nil
}

// tenants
func GetTenantConfigStruct(rawData []byte) (domain.TenantConfig, error) {
	var tenantConfig domain.TenantConfig
	if !json.Valid(rawData) {
		return tenantConfig, errors.New("invalid JSON syntax")
	}

	if err := json.Unmarshal(rawData, &tenantConfig); err != nil {
		return tenantConfig, err
	}
	return tenantConfig, nil
}
func GetJSONTenantConfigListFromStruct(t domain.TenantConfigList) (string, error) {
	jsonBytes, err := json.Marshal(t)
	if err != nil {
		return "", err
	}
	return string(jsonBytes), nil
}
func GetJSONTenantConfigFromStruct(t domain.TenantConfig) (string, error) {
	jsonBytes, err := json.Marshal(t)
	if err != nil {
		return "", err
	}
	return string(jsonBytes), nil
}

// service
func GetServiceConfigStruct(rawData []byte) (domain.ServiceConfig, error) {
	var serviceConfig domain.ServiceConfig
	if !json.Valid(rawData) {
		return serviceConfig, errors.New("invalid JSON syntax")
	}

	if err := json.Unmarshal(rawData, &serviceConfig); err != nil {
		return serviceConfig, err
	}
	return serviceConfig, nil
}
func GetJSONServiceConfigFromStruct(t domain.ServiceConfig) (string, error) {
	jsonBytes, err := json.Marshal(t)
	if err != nil {
		return "", err
	}
	return string(jsonBytes), nil
}
func GetJSONServiceConfigListFromStruct(t domain.ServiceConfigList) (string, error) {
	jsonBytes, err := json.Marshal(t)
	if err != nil {
		return "", err
	}
	return string(jsonBytes), nil
}

// shema
func GetJSONSchemaConfigFromStruct(t domain.SchemaConfig) (string, error) {
	jsonBytes, err := json.Marshal(t)
	if err != nil {
		return "", err
	}
	return string(jsonBytes), nil
}

func GetSchemaConfigStruct(rawData []byte) (domain.SchemaConfig, error) {
	var schemaConfig domain.SchemaConfig
	if !json.Valid(rawData) {
		return schemaConfig, errors.New("invalid JSON syntax")
	}

	if err := json.Unmarshal(rawData, &schemaConfig); err != nil {
		return schemaConfig, err
	}
	return schemaConfig, nil
}

func GetCurrentDateTime() string {
	currentTimeUTC := time.Now().UTC()
	romeLocation := time.FixedZone("Rome", 2*60*60)
	currentTimeRome := currentTimeUTC.In(romeLocation)
	iso8601Format := "2006-01-02T15:04:05+01:00"
	return currentTimeRome.Format(iso8601Format)
}
